#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

test_data = {"key": '358b2a22-5027-4f87-84f6-a3bfe2d0f0aa', "value": 1903}
headers = {'Content-Type': 'application/json' }

def test_healthcheck(app, client):
    url = '/healthcheck'
    res = client.get(url)
    assert res.status_code == 200

def test_add_to_redis(app, client):
    url = '/data'
    res = client.post(url, data=json.dumps(test_data), headers=headers)
    assert res.status_code == 200

def test_get_from_redis(app, client):
    url = '/data/358b2a22-5027-4f87-84f6-a3bfe2d0f0aa'
    res = client.get(url)
    assert res.status_code == 200
    assert "1903" in res.data.strip().decode()

def test_fail_to_get_person(app, client):
    url = '/people/12345'
    res = client.get(url, headers=headers)
    assert res.status_code == 404
