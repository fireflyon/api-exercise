import pytest
import os
import sys

myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../src/')

from src.app import app as data_app

@pytest.fixture
def app():
    yield data_app

@pytest.fixture
def client(app):
    return app.test_client()
