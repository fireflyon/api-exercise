#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask
import healthcheck
import routes

app = Flask(__name__)

app.add_url_rule('/healthcheck', view_func=healthcheck.health_check, methods=['GET'])
app.add_url_rule('/data', view_func=routes.add_data, methods=['POST'])
app.add_url_rule('/data/<key>', view_func=routes.get_data, methods=['GET'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
