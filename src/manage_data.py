import os
import redis
import fakeredis


class ManageData:
    redis_host = os.environ.get("REDIS_HOST", 'localhost')
    redis_port = os.environ.get("REDIS_PORT")
    redis_db = os.environ.get("REDIS_DB")
    if redis_host == 'localhost':
        r = fakeredis.FakeStrictRedis()
    else:
        r = redis.Redis(host=redis_host, port=redis_port, db=int(redis_db),
                        socket_connect_timeout=5, socket_timeout=5)

    @classmethod
    def get_from_redis(cls, key):
        try:
            d = cls.r.get(key).strip().decode()
        except (redis.ConnectionError, redis.TimeoutError) as e:
            print("DB Error: {}".format(str(e)))
            return False
        except AttributeError:
            print("Data not found")
            return False

        return d

    @classmethod
    def add_to_redis(cls, data):
        try:
            cls.r.set(data["key"], data["value"])
        except (redis.ConnectionError, redis.TimeoutError) as e:
            print("DB Error: {}".format(str(e)))
            return False

        return True
