from flask import request, make_response, jsonify
from manage_data import ManageData


def add_data():
    data = ManageData.add_to_redis(request.json)
    return json_response(data)


def get_data(key):
    data = ManageData.get_from_redis(key)
    return json_response(data)


def json_response(result, is_json=True):
    if result:
        if is_json:
            response = make_response(jsonify(result), 200,)
        else:
            response = make_response('OK', 200,)
    else:
        response = make_response('Not found', 404,)
    response.headers["Content-Type"] = "application/json"
    return response