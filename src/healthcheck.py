#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import make_response, jsonify


def health_check():
    response = make_response(jsonify({"app": {"status": "UP"}}), 200, )
    response.headers["Content-Type"] = "application/json"
    return response
