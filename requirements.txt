flask==1.1.2
redis==3.5.3
fakeredis==1.5.0
pytest==6.2.1
pytest-cov==2.11.1
pytest-randomly==3.5.0